color Display Background white
display depthcue off
axes location Off
for {set i 0} {$i < 5} {incr i} {
for {set j 0} {$j < 5} {incr j} {
for {set k 0} {$k < 5} {incr k} {
set total [expr {$i * 25 + $j * 5 + $k}]
if {[catch {mol load xyz cluster_${i}-${j}-${k}.xyz} res]} {
mol load xyz "cluster_${i}-${j}-${k}_i.xyz"
}
mol modstyle all $total "Licorice"
if {[expr $i %2] == 0 && [expr $j %2] == 0 && [expr $k %2] == 0} {
mol modcolor all $total "ColorID" 0
} else {
mol modmaterial all $total "Transparent"
mol modcolor all $total "ColorID" 10
}
}
}
}
mol top 62
display resetview
scale by 0.43
rotate z by 90
rotate y by 45
rotate x by 30

for {set x 0} {$x < 125} {incr x} {
mol off $x
}
for {set y 0} {$y < 125} {incr y} {
mol on $y
render snapshot f$y.tga convert %s -resize 6000x4000 -density 72 -quality 90 %s.png
}
