# Load the system
mol load pdb tartaric_wb.pdb dcd tartaric_md.dcd
# Set the Periodic Boundary Conditions box size
pbc set {30 30 30} -all
# Center the molecule of interest
pbc wrap -centersel "resname TAR" -center com -all
# Avoid all connection issues from the PBC
pbc join connected -all
# Select all frames from the MD
set nf [molinfo top get numframes]
# Iterate over all frames ...
for {set i 0} {$i < $nf} {incr i 500} {
 # Save each frame as a PDB file
 [atomselect top all frame $i] writepdb frame_$i.pdb
}
exit
