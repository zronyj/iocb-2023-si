import sys
import argparse as ap

pkmol_wat = """# https://pablito-playground.readthedocs.io/en/latest/tutorials/qmmm_amber_cpmd/packmol_custom_box.html

# The type of the files will be pdb
filetype pdb

# The name of the output file
output {name}.pdb

# add first type of solvent molecules
structure water.pdb
  number {mols}
  inside cube 0. 0. 0. {side:.2f}
  resnumbers 0
  chain X
end structure"""

pkmol_solv = """# https://pablito-playground.readthedocs.io/en/latest/tutorials/qmmm_amber_cpmd/packmol_custom_box.html

# All atoms from diferent molecules will be at least 2.0 Angstroms apart
tolerance 2.0

# The type of the files will be pdb
filetype pdb

# The name of the output file
output {name}_wb.pdb

# put the COM of the solute at the center of the box
structure {name}.pdb
  number 1
  fixed {midside}. {midside}. {midside}. 0. 0. 0.
  centerofmass
  chain X
end structure

# add first type of solvent molecules
structure water.pdb
  number {mols}
  inside cube 0. 0. 0. {side:.2f}
  resnumbers 0
  chain X
end structure"""

# Density of water (kg/m^3)
density = 997

# Molecular mass of water (g/mol)
mm = 18

# Molecules per mol
avogadro = 6.022E23

# The number of water molecules in a cubic meter
factor = density * 1000 / mm * avogadro

parser = ap.ArgumentParser(prog="Pre-PackMol",
    description="""
Just a small script to prepare the input for PackMol
in order to create a water box.

Keep in mind that the water molecule should be named
as 'water.pdb' and both that and the solute should be
in the same folder.
""",
epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('-n', '--name', type=str,
    help=("Specify the name of the molecule of interest."
            "It should be the name in the PDB file!"),
    required=True)
parser.add_argument('-s', '--side', type=float,
    help=("Specify the length of the box side. Remember "
    		"that it should be an even number."),
    required=True)
parser.add_argument('-v', '--molvol', type=float,
    help="Specify the volume of the solvated molecule.")

# Parsing all the command line arguments
args = parser.parse_args()

if args.molvol:
	# Get input from user
	side = args.side              # In Angstrom
	molvol = args.molvol          # In cubic Angstrom

	# Volume in cubic meters
	volume = (side*10**-10)**3

	# Volume of the molecule
	molvol *= (10**-10)**3

	# Molecules in the box
	mols = int(factor * (volume - molvol))

	print(f"For a box with side {side:.2f} A, the volume is {volume:.4e} m^3,")
	print(f"the volume of the box without the molecule is {volume - molvol:.4e} m^3,")
	print(f"and the number of water molecules is {mols}.")

	with open("packmol_input.inp", "w") as pkin:
		pkin.write(pkmol_solv.format(name=args.name, side=side, midside=int(side/2), mols=mols))

else:
	# Get input from user
	side = float(sys.argv[2])              # In Angstrom

	# Volume in cubic meters
	volume = (side*10**-10)**3

	# Molecules in the box
	mols = int(factor * volume)

	print(f"For a box with side {side:.2f} A, the volume is {volume:.4e} m^3")
	print(f"and the number of water molecules is {mols}.")

	with open("packmol_input.inp", "w") as pkin:
		pkin.write(pkmol_wat.format(name=args.name, side=side, mols=mols))