#!/bin/bash
#$ -q long
#$ -l num_proc=8
#$ -S /bin/bash
#$ -N cluster_114
#$ -o /share/zronyj/cluster_114.o
#$ -j y
#$ -r no
#$ -c n
#$ -V
export LD_LIBRARY_PATH=""
export PATH=/bin:/usr/bin:/usr/local/bin:/share/zronyj/bin
export WDIR=/share/zronyj
export SCRPATH=/scratch/zronyj
mkdir -p $SCRPATH/$JOB_NAME.$JOB_ID
export SIMDIR=$SCRPATH/$JOB_NAME.$JOB_ID

# Move all the data to scratch
mv $WDIR/$JOB_NAME/* $SIMDIR

# Go to the scratch folder
cd $SIMDIR/

# Get frequencies
if [ ! -f $JOB_NAME.chk ]; then
/usr/local/g16-A.03/g16 $clu.inp $clu.out
fi

# Extract the frequencies and other files
gar9 $JOB_NAME.out

# Get gradients 
/usr/local/g16-A.03/g16 G98.INP G98.OUT
qgrad

# Secondary loop for optimization
while [ -f "INP.NEW" ]; do
mv INP.NEW G98.INP
# Check that the G98.INP file is written correctly
python rephrase.py
cp G98.OUT G98.OUT.last
/usr/local/g16-A.03/g16 G98.INP G98.OUT
qgrad
done

# Run ROA calculation
/usr/local/g16-A.03/g16 FREE.INP FREE.OUT

# Go back to the home folder
cd $WDIR
# Move all the data back
mv $SIMDIR/* $WDIR/$JOB_NAME

echo $JOB_NAME
echo "OK"