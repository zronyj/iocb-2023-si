#!/bin/bash
#$ -q long
#$ -l num_proc=4
#$ -S /bin/bash

# Move all the data to scratch
mv $clu /scratch/zronyj

# Go to the scratch folder
cd /scratch/zronyj/$clu

# Get frequencies
if [ ! -f $clu.chk ]; then
/usr/local/g16-A.03/g16 $clu.inp $clu.out
fi

# Extract the frequencies and other files
gar9 $clu.out

# Get gradients 
/usr/local/g16-A.03/g16 G98.INP G98.OUT
qgrad

# Secondary loop for optimization
while [ -f "INP.NEW" ]; do
mv INP.NEW G98.INP
# Check that the G98.INP file is written correctly
python rephrase.py
cp G98.OUT G98.OUT.last
/usr/local/g16-A.03/g16 G98.INP G98.OUT
qgrad
done

# Run ROA calculation
/usr/local/g16-A.03/g16 FREE.INP FREE.OUT

# Go back to the home folder
cd $HOME
# Move all the data back
mv /scratch/zronyj/$clu .
