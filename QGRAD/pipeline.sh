#!/bin/bash

# Main loop to handle all available clusters
for d in $(ls -d */); do
clu=${d%?}

# Go to the scratch folder
cd $clu

# Get frequencies
if [ ! -f $clu.chk ]; then
/usr/local/g16-A.03/g16 $clu.inp $clu.out
fi

# Extract the frequencies and other files
/share/zronyj/gar9 $clu.out

# Get gradients 
/usr/local/g16-A.03/g16 G98.INP G98.OUT
/share/zronyj/qgrad

# Secondary loop for optimization
while [ -f "INP.NEW" ]; do
mv INP.NEW G98.INP
# Check that the G98.INP file is written correctly
python rephrase.py
cp G98.OUT G98.OUT.last
/usr/local/g16-A.03/g16 G98.INP G98.OUT
/share/zronyj/qgrad
done

# Run ROA calculation
/usr/local/g16-A.03/g16 FREE.INP FREE.OUT

# Go back to the home folder
cd ..

echo $clu
done