# Job: SPE and gradient of solvated tartaric acid
#
gpus 1
# basis set
basis 6-31g*
# coordinates file
coordinates tartaric.xyz
# molecule charge
charge 0
# SCF method (e.g. rhf/blyp/b3lyp/etc...)
method b3lyp
convthre 1.0e-2
# add dispersion correction (DFT-D)
dftd yes
# type of the job (e.g. energy/gradient/md/minimize/ts)
run gradient
end

