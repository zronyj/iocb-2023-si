import numpy as np
import argparse as ap
import MDAnalysis as mda

parser = ap.ArgumentParser(prog="Center Molecule in Box",
    description="""
Small script to center a solute in a solvent box
after an MD simulation.
""",
epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('--pdb', type=str,
    help="Specify the name of the PDB file to work with.",
    required=True)
parser.add_argument('--dcd', type=str,
    help="Specify the name of the DCD file to work with.",
    required=True)
parser.add_argument('-u', '--solute', type=str,
    help=("Specify the 3 letter identifier of the solute in the PDB file."),
    required=True)
parser.add_argument('-v', '--solvent', type=str,
    help=("Specify the 3 letter identifier of the solvent (water) in the PDB file."),
    required=True)

# Parsing all the command line arguments
args = parser.parse_args()

# Load data
u = mda.Universe(args.pdb, args.dcd, in_memory=True)

# Select solute
solu = u.select_atoms(f'resname {args.solute}')

# Move all the atoms from the solute to the center
for ts in u.trajectory:
	# Compute the center of mass of the solute
	solu_center = solu.center_of_mass()
	# Extract the dimensions of the box
	dim = ts.triclinic_dimensions
	# Compute the center of the box
	box_center = np.sum(dim, axis=0) / 2
	# Move all the atoms to the center of the solute
	u.atoms.translate(box_center - solu_center)

# Select solvent
solv = u.select_atoms(f'resname {args.solvent}')

# Move all the solvent molecules to wrap the molecule
for ts in u.trajectory:
	solv.wrap(compound='residues')

# Save the first state of the centered trajectory
u.atoms.write('center_traj.pdb')

# Save the trajectory with the centered solute
with mda.Writer('center_traj.dcd', u.atoms.n_atoms) as w:
    for ts in u.trajectory:
        w.write(u.atoms)