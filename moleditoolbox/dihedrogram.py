#!/usr/bin/env python3
import argparse  as ap
import numpy as np
import MDAnalysis as mda
from matplotlib import pyplot as plt

parser = ap.ArgumentParser(prog="Dihedral Finder",
    description="""
Small script to find the dihedral angle of a solute
given the identity and atom indeces.
""",
epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('--pdb', type=str,
    help="Specify the name of the PDB file to work with.",
    required=True)
parser.add_argument('--dcd', type=str,
    help="Specify the name of the DCD file to work with.",
    required=True)
parser.add_argument('-n', '--name', type=str,
    help=("Specify the name of the solute. Remember "
    		"that it should be the identifier in the"
    		"PDB file."),
    required=True)
parser.add_argument('-a', '--atoms', type=int, nargs=4,
    help="Sequence of atoms (0-based) of the dihedral.",
    required=True)

# Parsing all the command line arguments
args = parser.parse_args()

# Load data
u = mda.Universe(args.pdb, args.dcd)

# Select solute
res = u.select_atoms(f'resname {args.name}')

# Select atoms
atoms = res.select_atoms(f'index {args.atoms[0]}', f'index {args.atoms[1]}',
						f'index {args.atoms[2]}', f'index {args.atoms[3]}')

# Initialize dihedrals array
dihedrals = []

# Iterate over all frames ...
for s in range(u.trajectory.n_frames):
	u.trajectory[s]
	# ... and get all the dihedrals
	dihedrals.append(atoms.dihedral.value())

# Make the histogram
h, e = np.histogram(dihedrals, bins=int(u.trajectory.n_frames/100))

print(f"Maximum value at: {e[list(h).index(h.max())]:.2f}°")

# Build an X axis
x = np.arange(-180, 180, 360/len(h))

# Plot the whole thing
plt.plot(x, h, "r-")
plt.xlabel('Angle / °')
plt.ylabel('Number of occurrences')
plt.title(f'Histogram of dihedral angle for {args.pdb[:-4]}')
plt.savefig('histogram.png', dpi=600)
