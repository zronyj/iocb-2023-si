import os                              # A library to manage the file system
import argparse as ap                  # To take command line arguments nicely
import MDAnalysis as mda               # To analyze an MD trajectory
import MDAnalysis.transformations as trans # To transform the residues inside the solvation box

parser = ap.ArgumentParser(prog="Frame saver",
    description="""
Small script to extract the frames of a DCD trajectory
as PDB files.""",
    epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

    # Adding all the command line options to the program
parser.add_argument('-pdb', type=str,
    help="Specify the name of the PDB file to be used as a reference.",
    required=True)
parser.add_argument('-dcd', type=str,
    help="Specify the name of the DCD file with the trajectory.",
    required=True)
parser.add_argument('-s', '--step', type=int,
    help="Specify how many frames should be skipped between saving PDBs.")

# Parsing all the command line arguments
args = parser.parse_args()

# Where am I?
here = os.getcwd()

pdb = os.path.join(here, args.pdb)
dcd = os.path.join(here, args.dcd)

try:
    skip = args.step
except Exception as e:
    skip = 1

u = mda.Universe(pdb, dcd)

all_molecules = u.residues.resnames.tolist()
unique = set(all_molecules)

count = [[mol, all_molecules.count(mol)] for mol in unique]
count.sort(key=lambda x: x[1])
sel_mol = count[0][0]
sol_mol = count[-1][0]

molecule_of_interest = u.select_atoms(f"resname {sel_mol}")
solvent_molecules = u.select_atoms(f"resname {sol_mol}")

transforms = [trans.unwrap(molecule_of_interest),
              trans.center_in_box(molecule_of_interest, wrap=True),
              trans.wrap(solvent_molecules)]

u.trajectory.add_transformations(*transforms)

all_atoms = u.select_atoms('all')
for f in range(0, len(u.trajectory), skip):
    u.trajectory[i]
    all_atoms.write(f"frame_{i}.pdb", remarks=f"Frame {i}", bonds="conect")

