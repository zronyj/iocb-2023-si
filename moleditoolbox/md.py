import os
import argparse as ap
import MDAnalysis as mda
from matplotlib import pyplot as plt

parser = ap.ArgumentParser(prog="Solvation Shell",
	description="""
	Small Python script to find the number of solvent molecules
	in the solvation shell at a given distance of a molecule.""",
	epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('-t1', '--threshold1', type=float,
    help=("Specify which threshold to use around the molecule to include "
            "solvent molecules as the first solvation sphere."),
    required=True)
parser.add_argument('-t2', '--threshold2', type=float,
    help=("Specify which threshold to use around the molecule to include "
            "solvent molecules as the second solvation sphere."),
    required=True)
parser.add_argument('-n', '--name', type=str,
    help=("Specify the name of the molecule of interest."
            "It should be the name of the PDB file!"),
    required=True)
parser.add_argument('-S', '--solute', type=str,
    help=("Specify the 3 letter identifier of the solute in the PDB file."),
    required=True)
parser.add_argument('-s', '--solvent', type=str,
    help=("Specify the 3 letter identifier of the solvent (water) in the PDB file."),
    required=True)

# Parsing all the command line arguments
args = parser.parse_args()

name = args.name

here = os.getcwd()

pdb = os.path.join(here, f'{name}.pdb')
dcd = os.path.join(here, name, f'{name}.dcd')

u = mda.Universe(pdb,dcd)

tar = u.select_atoms(f'resname {args.solute}')
waters = u.select_atoms(f'resname {args.solvent}')

dynamic1 = u.select_atoms(f'around 3.1 resname {args.solute}', updating=True)
dynamic2 = u.select_atoms(f'around 5.0 resname {args.solute}', updating=True)

coll1 = []
coll2 = []

resids1 = []
resids2 = []

for f in u.trajectory:
	coll1.append(dynamic1.n_atoms / 3)
	coll2.append(dynamic2.n_atoms / 3 - coll1[-1])

	resids1.append(dynamic1.n_residues)
	resids2.append(dynamic2.n_residues - resids1[-1])

sphere1 = sum(coll1[20:])/(u.trajectory.n_frames - 20)
sphere2 = sum(coll2[20:])/(u.trajectory.n_frames - 20)

shell1 = sum(resids1[20:])/(u.trajectory.n_frames - 20)
shell2 = sum(resids2[20:])/(u.trajectory.n_frames - 20)

print(f"Number of solvent molecules in first sphere: {sphere1:.2f}")
print(f"Number of solvent molecules in second sphere: {sphere2:.2f}")

print(f"Number of residues in first sphere: {shell1:.2f}")
print(f"Number of residues in second sphere: {shell2:.2f}")

traj = [i * 500/1E6 for i in range(u.trajectory.n_frames)]

plt.plot(traj[20:], resids1[20:], "m-", label=r"$1^{st}$ solvation shell")
plt.plot(traj[20:], resids2[20:], "c-", label=r"$2^{nd}$ solvation shell")
plt.hlines(y=shell1, xmin=0, xmax=10, color='k', linestyle='-')
plt.hlines(y=shell2, xmin=0, xmax=10, color='w', linestyle='-')
plt.text(0, shell1+0.5, f"avg: {shell1}", fontsize=10)
plt.text(0, shell2+0.5, f"avg: {shell2}", fontsize=10)
plt.xlim([0,10])

plt.title(("Number of solvent molecules around the solute\n"
	r"at $3.1 \AA$ and $5.0 \AA$"), fontsize=14)

plt.xlabel("Time (ns)", fontsize=12)
plt.ylabel("Number of solvent molecules", fontsize=12)

plt.legend()

plt.savefig(f"{name}.png", dpi=300)