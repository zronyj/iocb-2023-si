import numpy as np
import argparse as ap
import MDAnalysis as mda

parser = ap.ArgumentParser(prog="MD Post-Processing",
    description="""
Script to read the molecule (PDB) and the trajectory (DCD), then
center the solvate molecule, and finally extract snapshots of the
trajectory and save them in PDB format (each).

The script may read other formats, but is subject to what is
made available by MDAnalysis [https://www.mdanalysis.org/].
""",
epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('--pdb', type=str,
    help="Specify the name of the PDB file to work with.",
    required=True)
parser.add_argument('--dcd', type=str,
    help="Specify the name of the DCD file to work with.",
    required=True)
parser.add_argument('-u', '--solute', type=str,
    help=("Specify the 3 letter identifier of the solute in the PDB file."),
    required=True)
parser.add_argument('-v', '--solvent', type=str,
    help=("Specify the 3 letter identifier of the solvent "
            "(water) in the PDB file."),
    required=True)
parser.add_argument('-n', '--snap_number', type=int,
    help="How many snapshots should be produced.")
parser.add_argument('-f', '--frequency', type=int,
    help="How many frames should there be between snapshots.")
parser.add_argument('-s', '--snapshots', nargs='+', type=int,
    help="List of snapshots that you wish to save.")

# Parsing all the command line arguments
args = parser.parse_args()

# ------------------------------> Main Program <------------------------------

# Load data into memory (both PDB and DCD file)
u = mda.Universe(args.pdb, args.dcd, in_memory=True)

# Select solute (should be the residue name -3 characters- in the PDB)
solu = u.select_atoms(f'resname {args.solute}')

# Check whether the solute is in the center of the box

# Compute the center of mass of the solute
solu_center = solu.center_of_mass()
# Extract the dimensions of the box
dim = u.trajectory[0].triclinic_dimensions
# Compute the center of the box
box_center = np.sum(dim, axis=0) / 2

# If the solute molecule is not in the center ...
if np.linalg.norm(box_center - solu_center) > 1E-3:

    # Move all the atoms from the solute to the center
    for ts in u.trajectory:
        # Compute the center of mass of the solute
        solu_center = solu.center_of_mass()
        # Extract the dimensions of the box
        dim = ts.triclinic_dimensions
        # Compute the center of the box
        box_center = np.sum(dim, axis=0) / 2
        # Move all the atoms to the center of the solute
        u.atoms.translate(box_center - solu_center)

    # Select solvent
    solv = u.select_atoms(f'resname {args.solvent}')

    # Move all the solvent molecules to wrap the molecule
    for ts in u.trajectory:
        solv.wrap(compound='residues')

    # Save the first state of the centered trajectory
    u.atoms.write('center_traj.pdb')

    # Save the trajectory with the centered solute
    with mda.Writer('center_traj.dcd', u.atoms.n_atoms) as w:
        for ts in u.trajectory:
            w.write(u.atoms)

# If a number of snapshots are required
if args.snap_number:
    # Set trajectory to 0
    u.trajectory[0]
    # Calculate the distance between snapshots
    delta = int(u.trajectory.n_frames / args.snap_number)
    # Iterate over frames every delta
    for i in range(delta, u.trajectory.n_frames, delta):
        # Set the frame
        u.trajectory[i]
        # Save the snapshot
        u.atoms.write(f'frame_{i}.pdb')

# If snapshots are required by frequency
if args.frequency:
    # Set trajectory to 0
    u.trajectory[0]
    # Iterate over frames every delta
    for i in range(args.frequency, u.trajectory.n_frames, args.frequency):
        # Set the frame
        u.trajectory[i]
        # Save the snapshot
        u.atoms.write(f'frame_{i}.pdb')

# If specific snapshots are required
if args.snapshots:
    # Set trajectory to 0
    u.trajectory[0]
    # Iterate over provided frames
    for i in args.snapshots:
        # Set the frame
        u.trajectory[i]
        # Save the snapshot
        u.atoms.write(f'frame_{i}.pdb')