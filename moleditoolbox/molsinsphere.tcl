mol new *.psf
mol addfile *.dcd
set frames [molinfo top get numframes]
set fp [open "waters.txt" w]
set protein "protein"
for {set i 0} {$i < $frames} {incr i} {
    puts "Frame: $i"
    set a [atomselect top "(water within 3 of ($protein) )" frame $i]
    set num [$a num]
    puts $fp "$i $num"
    $a delete
}
close $fp