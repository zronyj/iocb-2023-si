import argparse as ap
from matplotlib import pyplot as plt

raw = lambda s: fr"{s}"

parser = ap.ArgumentParser(prog="Plotter",
    description="""
Small script to plot data in a file. The data should be
in a two-column format.
""",
epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('-d', '--data', type=str,
    help="Specify the name of the file to work with.",
    required=True)
parser.add_argument('-t', '--title', type=str,
    help="Provide the title of the plot.")
parser.add_argument('-x', '--xaxis', type=str,
    help="Label to be included for the X axis.")
parser.add_argument('-y', '--yaxis', type=str,
    help="Label to be included for the Y axis.")

# Parsing all the command line arguments
args = parser.parse_args()

# Open the file, and read the data
with open(args.data, 'r') as f:
    data = f.readlines()

# Parse the data into pairs of floats
data = [ [float(q) for q in l.split()] for l in data ]

# Divide the data into X and Y values
X, Y = zip(*data)

# Plot the data
plt.plot(X, Y, "r-", label=args.data[:-4])
plt.title(raw(args.title))                     # Add the title
plt.xlabel(raw(args.xaxis))                    # Add the X-axis label
plt.ylabel(raw(args.yaxis))                    # Add the Y-axis label

# Save the plot
plt.savefig(f"{args.data[:-4]}.png", bbox_inches='tight', dpi=300)