#!/usr/bin/env python3
import os
import numpy as np                     # To do basic scientific computing
import argparse as ap                  # To take command line arguments nicely
from multiprocessing import Pool       # To parallelize the process
from moledito import Atom, Molecule, Cluster # To handle the atomic and molecular data

def load_cluster(file_name):

    clust_name = file_name[:-4]

    c = Cluster('a')
    c.read_pdb(file_name)
    c.fix_box()
    c.save_as_pdb(f"{clust_name}_fixed")

    return c

def box_dimensions(clust, nbox, noverlap):

    lims = clust.get_limits()

    box_x_side = lims['x'][2] / nbox
    box_y_side = lims['y'][2] / nbox
    box_z_side = lims['z'][2] / nbox

    pieces_per_box = noverlap + 1

    ovrl_x = box_x_side / pieces_per_box
    ovrl_y = box_y_side / pieces_per_box
    ovrl_z = box_z_side / pieces_per_box

    num_box_side = pieces_per_box * nbox

    print(f"Box size: {box_x_side} x {box_y_side} x {box_z_side}")
    print(f"There will be {num_box_side} boxes per side counting overlaps")
    print(f"And the final number of boxes will be {num_box_side**3}")

    box_coords = {}

    for k in range(num_box_side):
        for j in range(num_box_side):
            for i in range(num_box_side):
                box_coords[f"{i}-{j}-{k}"] = {
                            'x':tuple([i * ovrl_x, i * ovrl_x + box_x_side]),
                            'y':tuple([j * ovrl_y, j * ovrl_y + box_y_side]),
                            'z':tuple([k * ovrl_z, k * ovrl_z + box_z_side])
                                            }
    return box_coords

def mol_submap(mol_names1, mol_names2, map_name):

    matrix1 = range(1, len(mol_names1) + 1)
    matrix2 = []

    # Initialize a pointer
    pointer = 1

    # Iterate over all molecular names for each atom
    for moln in mol_names1:
        # If the name is in the selected atoms ...
        if moln in mol_names2:
            # ... append the pointer!
            matrix2.append(pointer)
            # ... (and increment it)
            pointer += 1
        # Otherwise ...
        else:
            # ... just add a 0
            matrix2.append(0)

    # Initialize the text for the file
    transfer_matrix = ""

    for id1, m1 in enumerate(matrix1):
        transfer_matrix += f"{m1:>3}"
        if id1 != 0 and (id1+1) % 20 == 0:
            transfer_matrix += "\n"
    
    transfer_matrix += "\n"
    for id2, m2 in enumerate(matrix2):
        transfer_matrix += f"{m2:>3}"
        if id2 != 0 and (id2+1) % 20 == 0:
            transfer_matrix += "\n"

    with open(f"map_{map_name}.dat", "w") as f:
        f.write(transfer_matrix)

def atom_submap(clust, mol_names1, mol_names2, map_name):

    matrix1 = range(1, clust._Cluster__natoms + 1)
    matrix2 = []

    # Initialize a pointer
    pointer = 1

    # Iterate over all molecular names for each atom
    for moln in mol_names1:
        # If the name is in the selected atoms ...
        if moln in mol_names2:
            # ... iterate over all atoms in the molecule
            for atom in clust.molecules[moln].get_coords():
                # ... append the pointer!
                matrix2.append(pointer)
                # ... (and increment it)
                pointer += 1
        # Otherwise ...
        else:
            # ... just add a 0
            matrix2.append(0)

    # Initialize the text for the file
    transfer_matrix = ""

    for id1, m1 in enumerate(matrix1):
        transfer_matrix += f"{m1:>3}"
        if id1 != 0 and (id1+1) % 20 == 0:
            transfer_matrix += "\n"
    
    transfer_matrix += "\n"
    for id2, m2 in enumerate(matrix2):
        transfer_matrix += f"{m2:>3}"
        if id2 != 0 and (id2+1) % 20 == 0:
            transfer_matrix += "\n"

    with open(f"map_{map_name}.dat", "w") as f:
        f.write(transfer_matrix)


def put_in_boxes(clust, idd, dim, interesting):

    if clust.is_in_box(interesting, dim):
        flag = "_i"
    else:
        flag = ""
    c = clust.sub_cluster(dim)
    c.save_as_xyz(f"cluster_{idd}{flag}")
    atom_submap(
            clust,
            list(clust.molecules.keys()),
            list(c.molecules.keys()),
            idd
                )


if __name__ == "__main__":

    boxes_per_side = 3
    overlapping_box = 1

    here = os.getcwd()

    pdb_files = os.listdir()

    pdb_files = [f for f in pdb_files if ".pdb" in f]

    for pdb in pdb_files:
        everything = load_cluster(pdb)
        dims = box_dimensions(everything, boxes_per_side, overlapping_box)
        args = [[everything, idd, dim, "TAR_0001"] for idd, dim in dims.items()]
        code = pdb[:-4]
        new_path = os.path.join(here, code)
        os.mkdir(new_path)
        os.chdir(new_path)
        with Pool() as p:
            p.starmap(put_in_boxes, args)
        # for arg in args:
        #     put_in_boxes(*arg)
        
        os.chdir(here)