#!/usr/bin/env python3
import os
import argparse  as ap
from moledito import Molecule
from moledito import PERIODIC_TABLE as pte

def make_filex(natoms, coords, molname):
    x_data = f" FILE.X generated for {molname} by the Gaussian Input File Creator\n"
    x_data += f"   {natoms:>10}\n"
    for c in coords:
        num = pte[c[0]]['number']
        x_data += f" {num:>2} {c[1]:>12.6f} {c[2]:>12.6f} {c[3]:>12.6f}"
        x_data += " 0 0 0 0 0 0 0.0\n"
    with open('FILE.X', 'w') as x:
        x.write(x_data)

def make_ftryinp(natoms, coords):
    ftry_data = "INTY" + "   0   1"*3 + "   0\n"
    
    ftry_1 = ""
    ftry_2 = ""
    ftry_3 = ""
    for i in range(natoms):
        ftry_1 += f"{0:>12.6f}"
        ftry_2 += f"{pte[coords[i][0]]['atomic_mass']:>12.6f}"
        ftry_3 += f"{0:>12.6f}{0:>12.6f}{0:>12.6f}"
        if (i+1)%6 == 0 or (i+1) == natoms: 
            ftry_1 += "\n"
            ftry_2 += "\n"
        if (i+1)%2 == 0 or (i+1) == natoms:
            ftry_3 += "\n"

    ftry_data += "   0\n"
    ftry_data += ftry_1
    ftry_data += "   1\n"
    ftry_data += ftry_2
    ftry_data += f" {natoms*3:>12}\n"
    ftry_data += ftry_3

    with open('FTRY.INP', 'w') as t:
        t.write(ftry_data)

def make_g98(ginput):
    g2 = ginput.replace('freq', 'force')
    g3 = g2.replace('6-311++G**', '6-31G**')
    with open('G98.INP', 'w') as g:
        g.write(g3)

parser = ap.ArgumentParser(prog="Gaussian Input File Creator",
    description="""
Small script to create Gaussian input files given the header
charge, multiplicity and coordinates. A footer is also possible.
The coordinates should be located as XYZ files in the same
directory as the one where this program is being run.
""",
epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

# Adding all the command line options to the program
parser.add_argument('-b', '--before', type=str,
    help="Specify the name of the file with the header.",
    required=True)
parser.add_argument('-c', '--charge', type=int,
    help="Specify the charge of the system.",
    required=True)
parser.add_argument('-m', '--multiplicity', type=int,
    help="Specify the multiplicity of the system.",
    required=True)
parser.add_argument('-a', '--after', type=str,
    help="Specify if there is a footer that should be added.")
parser.add_argument('-q', '--qgrad', action='store_true',
    help="Create remaining input files for QGRAD.")

# Parsing all the command line arguments
args = parser.parse_args()

# Get the current working directory
here = os.getcwd()

# Get all the XYZ files
xyz_files = os.listdir(here)
xyz_files = [f for f in xyz_files if ".xyz" in f]

# Read the header of the Gaussian input file
with open(args.before, 'r') as h:
    h_data = h.read()

# If there is a footer, read it
if args.after:
    with open(args.after, 'r') as f:
        f_data = f.read()

# Iterate over all XYZ files
for xyz in xyz_files:

    global_name = xyz[:-4].replace("-", "")

    m = Molecule(global_name)

    m.read_xyz(os.path.join(here, xyz))

    atoms = m.get_coords()

    there = os.path.join(here, global_name)

    input_file = ""

    input_file += h_data.format(name=global_name)

    input_file += f"\n{global_name}\n\n{args.charge} {args.multiplicity}\n"

    for a in atoms:
        input_file += f" {a[0]} {a[1]:>16.8f} {a[2]:>16.8f} {a[3]:>16.8f}\n"

    input_file += "\n"

    if args.after:
        input_file += f_data.format(name=global_name, charge=args.charge,
                                    multiplicity=args.multiplicity)

    os.mkdir(there)
    os.chdir(there)

    with open(f"{global_name}.inp", "w") as o:
        o.write(input_file)

    if args.qgrad:
        make_filex(m.get_atoms(), atoms, global_name)
        make_ftryinp(m.get_atoms(), atoms)
        make_g98(input_file)

    os.chdir(here)