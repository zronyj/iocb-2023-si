import os
import numpy as np
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

here = os.getcwd()

lvl = ['6-31Gss']
symb = 'so*x^D'
color = ["red", "turquoise", "black", "orange", "blueviolet", "blue"]

for i, l in enumerate(lvl):
    file_name = os.path.join(here, f'B3LYP_{l}_Scan', 'wd_scan.relaxscanact.dat')
    with open(file_name, 'r') as f:
        data = f.readlines()
    data = [j.split() for j in data]
    X, Y = zip(*data)
    X = [float(x) for x in X]
    Y = [float(y) for y in Y]
    Y = [y - min(Y) for y in Y]
    X = np.array(X)
    Y = np.array(Y)
    plt.plot(X, Y, f"{symb[i]}-", color=color[i], label=l)


plt.title(("Water dimer: relaxed scan over hydrogen bond\n"
            r"from $1.0 \AA$ to $3.0 \AA$"
            "\nBasis set: cc-pVTZ Medium: vacuum"))
plt.xlabel(r"Distance ($\AA$)")
plt.ylabel("Energy (Eh)")
plt.legend()
plt.show()